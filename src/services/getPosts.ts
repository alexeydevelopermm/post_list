import React from "react";
import axios,  {AxiosResponse, AxiosRequestConfig, Axios} from "axios"

const getPosts = (): Promise<AxiosResponse> => {
    const options: AxiosRequestConfig = {
        method: 'GET',
        url: 'https://jsonplaceholder.typicode.com/posts'
    };

    return axios.request(options);
}

export default getPosts;