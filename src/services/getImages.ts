import React from "react";
import axios,  {AxiosResponse, AxiosRequestConfig, Axios} from "axios"

const getImages = (): Promise<AxiosResponse> => {
    const options: AxiosRequestConfig = {
        method: 'GET',
        url: 'https://source.unsplash.com/1600x900/?news'
    };

    return axios.request(options);
}

export default getImages;
