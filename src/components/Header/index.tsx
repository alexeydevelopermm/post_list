import React from "react";
import { NavLink } from 'react-router-dom';

import  {routeMain as routMainPage} from '../pages/MainPage';


const Header = () => {
    return (
        <nav>
            <NavLink to={routMainPage()}>
                Главная
            </NavLink>
        </nav>
    );
}

export default Header;