import React from "react";
import {BrowserRouter, Routes, Route, Link, Navigate} from 'react-router-dom';

import MainPage, {routeMain as routMainPage} from "../pages/MainPage";
import DetailPage, {routeMain as routDetailPage} from "../pages/DetailPage";

import Header from "../Header";
import Footer from "../Footer";

const AppContent = () => {
  
  return (
    <div className="mainWrapper">
        <Header />
        <main>
          <Routes>
            <Route path = {routMainPage()} element = {<MainPage />}/>
            <Route path = {routDetailPage()} element = {<DetailPage/>}/>
            <Route path="*" element={<Navigate to={routMainPage()} />}
            />
          </Routes>
        </main>
        <Footer />
    </div>
  );
}

export default AppContent;