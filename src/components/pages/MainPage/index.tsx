import React, {useState, useEffect} from "react";
import { IPostDetail } from "../../types/IPostDetail";


import PostList from "../../PostList";
import routeMain from "./routes";
import getPosts from "../../../services/getPosts";


const MainPage = () => {
    const [postlist, setPostList] = useState<IPostDetail[]>([]);

    useEffect(() => {
        getPosts().then((response) => {
            setPostList(response.data);
        })
    }, [])

    return (
        <PostList list={postlist}/>
    );
}

export {routeMain};
export default MainPage;