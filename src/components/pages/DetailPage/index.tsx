import { useEffect, useState } from "react";

import {useParams } from "react-router-dom";
import { IPostDetail } from "../../types/IPostDetail";
import getImages from "../../../services/getImages";
import routeMain from "./routes";
import getPosts from "../../../services/getPosts";

import "./style.scss";

const DetailPage = () => {
    const {id} = useParams<string>();
    const [post, setPost] = useState<IPostDetail | null>(null);
    const [image, setImage] = useState<any>(null);
    useEffect(() => {
        getPosts().then(response => {
            const selectedPost = response.data?.find((item: IPostDetail) => (item.id + '') == id);
            setPost(selectedPost);
        });
        getImages().then(response => {
            setImage(response.request.responseURL);
        })
    },[id]);

    return(
        <div className="post-detail">
            {post && image ? (
                <div className="post">
                    <img className="post__img" src={image} alt="" />
                    <h2 className="header__post">{post.title}</h2>
                    <p className="post__body">{post.body}</p>                 
                </div>
            ) : <></>}
                      
        </div>

    );

}
export {routeMain};
export default DetailPage;
