import React from "react";
import PostItem from "./PostItem";


import { IPostDetail } from "../types/IPostDetail";
import './style.scss'

interface IPosts{
    list: IPostDetail[];
}

const PostList: React.FC<IPosts> = ({list}) => {
    return(
        <div className="postlist">
            
            {list.map((list) => <PostItem key={list.id} userId={list.userId} id={list.id} title={list.title} body={list.body}/>)  }
        </div>

    );

}
export default PostList;