import React from "react";
import getImages from "../../../services/getImages";
import { NavLink } from "react-router-dom";
import {routeMain as routeDetailPage} from "../../pages/DetailPage"
import { IPostDetail } from "../../types/IPostDetail";


const PostItem:  React.FC<IPostDetail> = ({userId, id, title, body}) => {

    return(
        <div className = {"post-item-wrapper"}>
            <div className={"post-item"} >
                <div className="post" data-id={id}>
                    <h3>{title}</h3>
                    <p>{body}</p>          
                </div>            
            </div>  
            <NavLink className={"text-link"} to={ routeDetailPage('' + id)}>Посмотреть</NavLink>
        </div>



    );
}

export default PostItem;