import React from 'react';
import MainPage from '../pages/MainPage';
import AppContent from '../appContent';

import './App.css';

function App() {
  return (
    <div className="App">
        <AppContent/>
    </div>
  );
}

export default App;
